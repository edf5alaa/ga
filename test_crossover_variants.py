#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import argparse
from time import time, sleep

import numpy as np
import matplotlib.pyplot as plt

import crossover
from ht_individus import Individu
from ga_core import GA


def calc(lst):
    a = np.array(lst, dtype=float)
    avg = np.average(a, axis=0)
    return avg, avg - np.amin(a, axis=0), np.amax(a, axis=0) - avg


def get_scores(lst):
    max_lst, min_lst, avg_lst = [], [], []
    for results in lst:
        maxlst, minlst, avglst = zip(*results)
        max_lst.append(list(maxlst))
        min_lst.append(list(minlst))
        avg_lst.append(list(avglst))
    return [calc(lst) for lst in [max_lst, avg_lst, min_lst]]


def plot_results(title, results_v1, results_v2, fmt):
    fig, axes = plt.subplots(1, 3, sharey=True)
    plt.ylim(20, 220)
    axes[0].set_ylabel('scores')
    stitles = ['max score', 'average', 'min score']

    for i, scores in enumerate(zip(results_v1, results_v2)):
        xlst = list(range(len(scores[0][0])))
        axes[i].set_xlim(0, xlst[-1])
        axes[i].set_xlabel('generation')
        axes[i].set_title(stitles[i])
        for j, (avg, lmin, lmax) in enumerate(scores, 1):
            axes[i].errorbar(x=xlst, y=avg, yerr=[lmin, lmax], elinewidth=0.2, capsize=2, label='method v{}'.format(j))

    plt.suptitle(title)
    plt.legend(loc='lower right')
    fig.set_size_inches(30, 8)
    plt.subplots_adjust(left=0.05, right=0.96,wspace=0.07)
    plt.savefig(title + '.' + fmt)
    plt.close(fig)



ALL_CUTS_POINTS  = range(Individu.M * Individu.BIT_LEN + 1)
GENE_CUTS_POINTS = range(0, Individu.M * Individu.BIT_LEN + 1, Individu.BIT_LEN)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run fully parametrized test.')
    parser.add_argument('-pop-size', type=int, required=True, 
        help='set population\'s size')
    parser.add_argument('-ngen', type=int, required=True, 
        help='set the number of generations')
    parser.add_argument('-ntests', type=int, default=8, 
        help='set the number of tests to make (default value: %(default)s)')
    parser.add_argument('-pcross', type=float, default=0.95, 
        help='set crossover probability (default value: %(default)s)')
    parser.add_argument('-pmutat', type=float, default=0.002,
        help='set mutation probability (default value: %(default)s)')
    parser.add_argument('-selection-method', choices=['roulette', 'stochastic', 'tournament', 'rank'], required=True, 
        help='set the selection method to use')
    parser.add_argument('-cross-method', choices=['1p', '2p', 'uniform'], required=True,
        help='set the crossover method to use')
    parser.add_argument('-format', choices=['png', 'pdf', 'svg'], default='png',
        help='set output format')
    #parser.add_argument('-save', help='save this experiment in <file>')

    args = parser.parse_args(sys.argv[1:])

    Individu.PCROSS = args.pcross   # hack: directly set this value in class attributes
    Individu.PMUTAT = args.pmutat   # hack: directly set this value in class attributes
    selection_method = {'roulette'  : GA.selection1, 
                        'stochastic': GA.selection2, 
                        'tournament': GA.selection3, 
                        'rank'      : GA.selection4} [args.selection_method]
    cross_method = {'1p'     : crossover.cross1, 
                    '2p'     : crossover.cross2, 
                    'uniform': crossover.cross3} [args.cross_method]

    results = []
    
    initial_pop = [Individu() for _ in range(args.pop_size)]
    
    print('settings: {}, {}'.format(args.selection_method, args.cross_method))
    for i, cuts_points in enumerate([ALL_CUTS_POINTS, GENE_CUTS_POINTS], 1):
        print('   testing variant', i)
        generator = lambda p1, p2: Individu.make_individus(p1, p2, cross_method, cuts_points)
        stats = []
        for j in range(args.ntests):
            ga = GA(initial_pop, generator, selection_method)
            t1 = time()
            ga.run(args.ngen)
            t2 = time()
            best = ga.best
            stats.append(ga.stats)
            print('\tpass {} : best score is {}, in {:.5} seconds'.\
                        format(j + 1, best.score, t2 - t1))
            # sleep(2)
        results.append(stats)
    
    title = '{}-{}'.format(selection_method.__doc__, cross_method.__doc__)
    plot_results(title, *(get_scores(result) for result in results), fmt=args.format)
