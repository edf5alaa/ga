#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import subprocess
from itertools import product
from collections import namedtuple
from time import sleep

methods = {
    '-selection-method' : ['roulette', 'stochastic', 'tournament', 'rank'],
    '-cross-method' : ['1p', '2p', 'uniform']
}

Params = namedtuple('Params', ['ngen', 'psize', 'pmutat'])

parameters = {
    'roulette'  : Params(ngen='150', psize='200', pmutat='0.002'),
    'stochastic': Params(ngen='180', psize='200', pmutat='0.001'),
    'tournament': Params(ngen='100', psize='300', pmutat='0.008'),
    'rank'      : Params(ngen='100', psize='250', pmutat='0.005')
}

if __name__ == '__main__':
    for selmethod, crossmethod in product(methods['-selection-method'], methods['-cross-method']):
        params = parameters[selmethod]
        args = ['./test_crossover_variants.py', 
                    '-pop-size', params.psize, 
                    '-ngen', params.ngen,
                    '-pmutat', params.pmutat,
                    '-pcross', '0.95',
                    '-selection-method', selmethod, 
                    '-cross-method', crossmethod,
                    '-ntests', '10',
                    '-format', 'png']
        subprocess.call(args)
        sleep(6)    # cooling cpu ;)
