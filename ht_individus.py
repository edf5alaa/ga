#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random
from functools import reduce
from collections import defaultdict
import array
from dataset import pb2 as pb



def bitarray(seq):
    return array.array('B', [int(v) for v in seq])


def flatten(seqlst):
    return reduce(lambda x, y: x + y, seqlst)


def int2bin(n, sz=None):
    s = bin(n)[2:]
    if sz is None:
        return bitarray(s)
    return bitarray((sz - len(s)) * '0' + bin(n)[2:])

def bin2int(ba):
    ret = 0
    for v in ba:
        ret <<= 1
        ret += v
    return ret


class Individu:
    N = len(pb)
    M = len(pb[0])
    BIT_LEN = (M - 1).bit_length()
    PCROSS = 0.95   # default value
    PMUTAT = 0.002  # default value

    def __init__(self, chromosom=None):
        if chromosom is None:
            genes = list(range(Individu.M))
            random.shuffle(genes)
            chromosom = flatten([int2bin(n, sz=Individu.BIT_LEN) for n in genes])
        self.chromosom = chromosom
        self.score = self.decode()

    def solution(self):
        ret = []
        for pos in range(0, len(self.chromosom), Individu.BIT_LEN):
            ret.append(bin2int(self.chromosom[pos:pos+Individu.BIT_LEN]))
        return ret

    def decode(self):
        dct = defaultdict(list)
        penalty = 0
        for i, j in enumerate(self.solution()):
            if j not in range(Individu.M):
                penalty += 10
                continue
            dct[j].append(pb[i][j])
        return max(sum(max(lst) for lst in dct.values()) - penalty, 0)
        
    @staticmethod
    def make_individus(p1, p2, cross_method, cut_points):
        if random.random() < Individu.PCROSS:
            fils1, fils2 = cross_method(p1.chromosom, p2.chromosom, cut_points)
        else:
            fils1, fils2 = p1.chromosom[:], p2.chromosom[:]

        for ind in [fils1, fils2]:
            for i in range(len(ind)):
                if random.random() > Individu.PMUTAT:
                    continue
                ind[i] ^= 1 # flip bit

        return [Individu(fils1), Individu(fils2)]


#~ if __name__ == '__main__':
    #~ from time import time
    #~ import crossover
    #~ from ga_core import GA
    #~ 
    #~ sel_methods = { 'roulette'  : GA.selection1, 
                    #~ 'stochastic': GA.selection2, 
                    #~ 'tournament': GA.selection3, 
                    #~ 'rank'      : GA.selection4}
    #~ cross_methods = {'1p'     : crossover.cross1, 
                     #~ '2p'     : crossover.cross2, 
                     #~ 'uniform': crossover.cross3}
#~ 
    #~ POP_SIZE = 300
    #~ NGEN = 100
#~ 
    #~ Individu.PMUTAT = 0.002    #hack
#~ 
    #~ ALL_CUTS_POINTS  = range(Individu.M * Individu.BIT_LEN + 1)
    #~ GENE_CUTS_POINTS = range(0, Individu.M * Individu.BIT_LEN + 1, Individu.BIT_LEN)
    #~ 
    #~ initial_pop = [Individu() for _ in range(POP_SIZE)]
    #~ generator = lambda p1, p2: Individu.make_individus(p1, p2, cross_methods['2p'], GENE_CUTS_POINTS)
    #~ ga = GA(initial_pop, generator, sel_methods['roulette'])
    #~ t1 = time()
    #~ ga.run(NGEN)
    #~ t2 = time()
    #~ best = ga.best
    #~ print('best score is {}, in {:.5} seconds'.format(best.score, t2 - t1))
    #~ ga.plot_stats()
