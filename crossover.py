#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random

# range(step, len(seq1), step)
def cross1(seq1, seq2, cut_points):
    "one point crossover"
    #assert len(seq1) == len(seq2)
    x = random.choice(cut_points[1:-1])
    yield seq1[:x] + seq2[x:]
    yield seq2[:x] + seq1[x:]

# range(0, len(seq1) + 1, step)
def cross2(seq1, seq2, cut_points):
    "two points crossover"
    #assert len(seq1) == len(seq2)
    x1, x2 = sorted(random.sample(cut_points, 2))
    yield seq1[:x1] + seq2[x1:x2] + seq1[x2:]
    yield seq2[:x1] + seq1[x1:x2] + seq2[x2:]

# range(0, len(seq1) + 1, step)
def cross3(seq1, seq2, cut_points):
    "uniform crossover"
    l1, l2 = seq1[:], seq2[:]
    for i, pos2 in enumerate(cut_points[1:], 1):
        pos1 = cut_points[i-1]
        if random.random() > 0.5:
            l1[pos1:pos2] = seq2[pos1:pos2]
            l2[pos1:pos2] = seq1[pos1:pos2]
    yield l1
    yield l2
