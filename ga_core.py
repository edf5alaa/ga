#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import random
from math import fsum
import numpy as np

try:
    import matplotlib.pyplot as plt
    HAVING_PLOT = True
except ImportError as err:
    print('\twarning:', repr(err), file=sys.stderr)
    HAVING_PLOT = False

try:
    from PIL import Image, ImageDraw
    HAVING_PIL = True
except ImportError as err:
    print('\twarning:', repr(err), file=sys.stderr)
    HAVING_PIL = False


def pick(acc_probs, population):
    return population[np.searchsorted(acc_probs, random.random() * acc_probs[-1])]


def scale(pop, max_fit, min_fit, avg):
    FM = 2.
    if min_fit > (FM * avg - max_fit) / (FM - 1):
        delta = max_fit - avg
        slope = (FM - 1) * avg / delta
        offset = avg * (max_fit - FM * avg) / delta
    else:
        delta = avg - min_fit
        slope = avg / delta
        offset = -min_fit * avg / delta
    return np.array([slope * fit + offset for fit in pop])


class GA:
    def __init__(self, population, fmake_indiv, fselect):
        self.make_individus = fmake_indiv
        self.fselect = fselect
        self.population = population
        self.best = None
        self.stats = []
        self.score_history = []
        self.gene_history = []

    @staticmethod
    def reproduction(genitors, gen):
        random.shuffle(genitors)
        ret = []
        for parent1, parent2 in zip(*[iter(genitors)] * 2):
            ret.extend(gen(parent1, parent2))
        return ret

    def run(self, max_gen):
        self.collect()
        for gen in range(max_gen):
            mating_pool = self.fselect(self, self.population)
            self.population = self.reproduction(mating_pool, self.make_individus)
            self.collect()

    def collect(self):
        self.scores = np.array([ind.score for ind in self.population])
        self.max_score = self.scores.max()
        best_solution = self.population[self.scores.argmax()]
        if self.best is None or best_solution.score > self.best.score:
            self.best = best_solution
        self.min_score = self.scores.min()
        self.avg_score = self.scores.mean()
        #self.fitness = self.scores / self.avg_score
        #self.fitness_avg = self.fitness.mean() / len(self.fitness)
        self.make_stats()

    def make_stats(self):
        self.stats.append((self.max_score, self.min_score, self.avg_score))
        self.score_history.append(self.scores)
        self.gene_history.append([ind.chromosom for ind in self.population])
        
    def plot_stats(self, title=''):
        "plot scores evolution"
        if not HAVING_PLOT:
            print("\tplotting disabled")
            return

        fig = plt.figure()
        plt.plot([sl[0] for sl in self.stats], label='max')
        plt.plot([sl[1] for sl in self.stats], label='min')
        plt.plot([sl[2] for sl in self.stats], label='average')
        plt.title(title)
        plt.ylabel('fitness')
        plt.xlabel('generation')
        plt.legend(loc='lower right')
        plt.show()
        plt.close(fig)

    def plot_hists(self, nshots=15, title=''):
        "generate and save histograms"
        if not HAVING_PLOT:
            print("\tplotting disabled")
            return

        step = len(self.score_history) // nshots
        if step == 0: return

        max_score = self.best.score
        min_score = min(min(pop) for pop in self.score_history)
        max_popsize = max(len(pop) for pop in self.score_history)
        for i, pop_fit in enumerate(self.score_history[::step]):
            fig = plt.figure()
            plt.ylabel('nb individus')
            plt.xlabel('scores')
            plt.xlim(min_score, max_score)
            plt.ylim(0, max_popsize)
            plt.hist(pop_fit, bins=20)
            _title = '{} - generation {}'.format(title, i * step + 1)
            plt.title(_title)
            plt.savefig(_title + '.png')
            plt.close(fig)

    def plot_genotypes(self, nshots=20, title=''):
        if not HAVING_PIL:
            print('this feature is disabled, please install PIL library')
            return

        hist_sz = len(self.gene_history)
        step = hist_sz // nshots
        if step == 0: return
        
        point = 2, 2
        for idx in range(0, hist_sz, step):
            pop = self.gene_history[idx]
            pop_sz, genotype_sz = len(pop), len(pop[0])
            
            box = point[0] * genotype_sz, point[1] * pop_sz
            image = Image.new('RGB', box, "white")
            draw = ImageDraw.Draw(image)
            for j, chromosom in enumerate(pop):
                for i, v in enumerate(chromosom):
                    if v:
                        bbox = [point[0] * i, point[1] * j]
                        bbox += [bbox[0] + point[0], bbox[1] + point[1]]
                        draw.rectangle(bbox, fill='black')
            del draw
            with open(title + '_g{}.png'.format(idx), 'wb') as fp:
                image.save(fp, "PNG")

    def selection1(self, population):
        "roulette wheel with linear scaling"
        scaled = scale(self.scores, self.max_score, self.min_score, self.avg_score)
        acc = scaled.cumsum()
        keep = [pick(acc, population) for i in range(len(population))]
        return keep

    def selection2(self, population):
        "stochastic universal sampling"
        fitness = self.scores / self.avg_score
        acc = fitness.cumsum()
        #scaled = scale(fitness, max(fitness), min(fitness), 1.)
        #acc = scaled.cumsum()
        start = random.random()
        pointers = [start + i for i in range(len(population))]
        #pointers = np.arange(random.random(), len(population))
        keep = [population[i] for i in np.searchsorted(acc, pointers)]
        return keep

    def selection3(self, population):
        "tournament selection"
        keep = []
        for i in range(len(population)):
            candidates = random.sample(population, 2)
            keep.append(max(candidates, key=lambda ind: ind.score))
        return keep

    def selection4(self, population):
        "rank selection"
        sorted_pop = sorted(population, key=lambda ind: ind.score)
        ranks = np.arange(1, len(population) + 1)
        acc = ranks.cumsum()
        keep = [pick(acc, sorted_pop) for i in range(len(population))]
        return keep

    selection_method = [selection1, selection2, selection3, selection4]
